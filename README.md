Let's go on [MongoDB playground(https://mongoplayground.net/)](https://mongoplayground.net/)

I propose you, 3 katas to pratice MongoDB find and aggregate :

- [Navigate in code report](01-find/navigate-in-code-report.md): Let you discover the basic of `find` method.
- [Extract tags](02-aggregate/extract-tags.md): Use the aggregation pipeline to extract tags from content.
- [Play with the two collections](02-aggregate/using-two-collections.md): Use the aggregation pipeline to link documents from two collections.
