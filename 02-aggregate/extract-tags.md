## Result to get

Given the collection above, you should extract tag documents :
```json
{
    "name": "front-end",
    "audience": 12,
    "number": 3,
    "sessions":[
        {
            "name":"Learn Git instruction",
            "date": "13/06/2020",
            "audience":4
        },
        {
            "name":"Learn Git instruction",
            "date": "17/06/2020",
            "audience":5
        },
        {
            "name":"Learn Git instruction",
            "date": "24/06/2020",
            "audience":3
        }
    ]
}

```

## Starting collection of documents
The collection - to be copied-pasted to configuration part of Mongo playground:

```json
[
  {
    "session": "Learn Git instruction",
    "date": "13/06/2020",
    "tags":["git","coding","source control","back-end","front-end"],
    "audience":4
  },
  {
    "session": "Learn Git instruction",
    "date": "17/06/2020",
    "tags":["git","coding","source control","back-end","front-end"],
    "audience":5
  },
  {
    "session": "Learn Git instruction",
    "date": "24/06/2020",
    "tags":["git","coding","source control","back-end","front-end"],
    "audience":3
  },
  {
    "session": "MongoDB aggregate",
    "date": "26/06/2020",
    "tags":["mongodb","database","back-end"],
    "audience":2
  },
  {
    "session": "MongoDB aggregate",
    "date": "03/07/2020",
    "tags":["mongodb","database","back-end"],
    "audience":3
  }
]

```

## Hints 

**Hint 1:** The aggregation pipeline should be always valid JSON array.

**Hint 2:** Have a look to the [unwind](https://docs.mongodb.com/manual/reference/operator/aggregation/unwind/index.html) and [group](https://docs.mongodb.com/manual/reference/operator/aggregation/group/index.html) aggregation pipeline stages
