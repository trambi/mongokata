## Result to get

Given the collections above, you should create this document :
```json
{
  "departure": "2020-06-30 19:43",
  "arrival": "2020-06-30 21:01",
  "flight_number": "EJU9056",
  "airline": "easyJet",
  "aircraft": "A319",
  "origin_airport_name": "Paris-Charles-de-Gaulle",
  "origin_airport_iata": "CDG",
  "origin_airport_icao": "LFPG",
  "destination_airport_name": "Berlin-Schönefeld",
  "destination_airport_iata": "SXF",
  "destination_airport_icao": "EDDB"
}

```


## Starting collection of documents
The collections - to be copied-pasted to configuration part of Mongo playground (as there are two collections, we have to select `bson multiple collections` template):
```js
db={
  "flight": [
    {
      "origin_airport": "LFPG",
      "destination_airport": "EDDB",
      "departure": "2020-06-30 19:43",
      "arrival": "2020-06-30 21:01",
      "flight_number": "EJU9056",
      "airline": "easyJet",
      "aircraft": "A319"
    },
    {
      "origin_airport": "LFPG",
      "destination_airport": "EDDB",
      "departure": "2020-06-26 10:29",
      "arrival": "2020-06-26 11:59",
      "flight_number": "AHO348Y",
      "airline": "Air Hamburg",
      "aircraft": "C56X"
    },
    {
      "origin_airport": "LFPG",
      "destination_airport": "LFMN",
      "departure": "2020-06-26 05:20",
      "arrival": "2020-06-26 06:55",
      "flight_number": "AFR9790",
      "airline": "Air France",
      "aircraft": "A321"
    },
    {
      "origin_airport": "LFPG",
      "destination_airport": "LFMN",
      "departure": "2020-06-26 10:25",
      "flight_number": "AFR7710",
      "airline": "Air France",
      "aircraft": "A321"
    },
    {
      "origin_airport": "LFMN",
      "destination_airport": "LFPG",
      "departure": "2020-06-26 07:55",
      "arrival": "2020-06-26 10:30",
      "flight_number": "AFR9471",
      "airline": "Air France",
      "aircraft": "A321"
    },
    {
      "origin_airport": "LFPG",
      "destination_airport": "LEBL",
      "departure": "2020-06-26 02:43",
      "arrival": "2020-07-02 04:07",
      "flight_number": "FDX5173",
      "airline": "FedEx",
      "aircraft": "B752"
    },
    
  ],
  "airport": [
    {
      "icao": "LFPG",
      "iata": "CDG",
      "name": "Paris-Charles-de-Gaulle",
      "runways": [
        {
          "code": [
            "09L",
            "27R"
          ],
          "surface": "Asphalt",
          "TODA": 2760,
          "ASDA": 2700,
          "LDA": 2700
        },
        {
          "code": [
            "09R",
            "27L"
          ],
          "surface": "Asphalt",
          "TODA": 4260,
          "ASDA": 4200,
          "LDA": 4200
        },
        {
          "code": [
            "08L",
            "26R"
          ],
          "surface": "Asphalt",
          "TODA": 4202,
          "ASDA": 4142,
          "LDA": 4142
        },
        {
          "code": [
            "08R",
            "26L"
          ],
          "surface": "Asphalt",
          "TODA": 2760,
          "ASDA": 2700,
          "LDA": 2700
        }
      ]
    },
    {
      "icao": "EDDB",
      "iata": "SXF",
      "name": "Berlin-Schönefeld",
      "runways": [
        {
          "code": [
            "07R",
            "25L"
          ],
          "surface": "Asphalt",
          "TODA": 3660,
          "ASDA": 3600,
          "LDA": 3600
        },
        {
          "code": [
            "07L",
            "25R"
          ],
          "surface": "Asphalt",
          "TODA": 3660,
          "ASDA": 3600,
          "LDA": 3600
        }
      ]
    },
    {
      "icao": "EGKK",
      "iata": "LGW",
      "name": "Gatwick Airport",
      "runways": [
        {
          "code": [
            "08R",
            "26L"
          ],
          "surface": "Asphalt",
          "TODA": 3316,
          "ASDA": 3300,
          "LDA": 3300
        },
        {
          "code": [
            "08L",
            "26R"
          ],
          "surface": "Asphalt",
          "TODA": 2560,
          "ASDA": 2500,
          "LDA": 2500
        }
      ]
    },
    {
      "icao": "LFSB",
      "iata": [
        "BSL",
        "MLH",
        "EAP"
      ],
      "name": "Basel-Mulhouse-Freiburg",
      "runways": [
        {
          "code": [
            "15",
            "33"
          ],
          "surface": "Asphalt",
          "TODA": 3900,
          "ASDA": 3840,
          "LDA": 3840
        },
        {
          "code": [
            "08L",
            "026"
          ],
          "surface": "Asphalt",
          "TODA": 1820,
          "ASDA": 1800,
          "LDA": 1800
        }
      ]
    }
  ]
}
```

## Hints 

**Hint 1:** The aggregation pipeline should be always valid JSON array.

**Hint 2:** Have a look to the [lookup](https://docs.mongodb.com/manual/reference/operator/aggregation/lookup/index.html) aggregation pipeline stage.
