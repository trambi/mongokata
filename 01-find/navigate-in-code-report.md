Using Mongo Playground, you must navigate in the collection code_report.json.

Using `db.collection.find` method, you must find:
 
 - The errors with a severity `major`;
 - The errors about `Function1` or `Function2`;
 - The errors with a severity other than `info`.

__Hint 1__: The argument of method `find` should be always valid JSON documents.

__Hint 2__: Be sure to add a tab opened at the MongoDB documentation [Query and Projection Operators](https://docs.mongodb.com/manual/reference/operator/query/)

